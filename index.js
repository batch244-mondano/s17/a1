/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	
	let printDetails = function(){
	let fullName = prompt("what is your name?"); 
	let age = prompt("How old are you?"); 
	let location = prompt("Where do you live?");
	alert("Thank you for your input!");

	console.log('Hello, ' + fullName + '!');
	console.log('You are ' + age + ' years old.');
	console.log('You live in ' + location);
};
printDetails();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function faveBand(){
		console.log('1 Parokya ni Edgar');
		console.log('2 Kamikazee');
		console.log('3 Mayonnaise');
		console.log('4 Eraserheads');
		console.log('5 Callalily');
	};

	faveBand();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function faveMovies(){
		console.log('1 Braveheart');
		console.log('Rotten Tomatoes Rating: 85%');
		console.log('2 A Knights Tale');
		console.log('Rotten Tomatoes Rating: 79%');
		console.log('3 American Underdog');
		console.log('Rotten Tomatoes Rating: 98%');
		console.log('4 A Dogs Journey');
		console.log('Rotten Tomatoes Rating: 91%');
		console.log('5 Avatar');
		console.log('Rotten Tomatoes Rating: 82%');
	};

	faveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();